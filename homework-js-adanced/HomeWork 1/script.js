class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        return this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        return this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        return this._salary = newSalary;
    }
}

class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name,age,salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(newSalary) {
        return this._salary = newSalary;
    }
}

const Nik = new Programmer('Nik', 23, 2500, 'middle front-end');
const Vlad = new Programmer('Vlad', 6, 2000, 'junior c++ developer');
const Dima = new Programmer('Dima', 28, 2900, 'PHP developer');

console.log(Nik);
console.log(Vlad);
console.log(Dima);