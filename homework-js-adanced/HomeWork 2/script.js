const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегурочка и Мороз",
    },
    {
        author: "Дарья Дарья",
        name: "Да, я Дарья",

    },
    {
        author: "Тарас Шевченко",
        name: "Кобзар",
        price: 100
    }

];


const div = document.getElementById('root');
let ul = document.createElement('ul');
div.append(ul);


let li = document.createElement('li');
books.map((el) => {
    const {author, name, price} = el;
    if (author && name && price) {
        li.insertAdjacentHTML("beforeend", `<li>Автор: ${author}.<br> Название: ${name},<br> Стоимость: ${price}.</li>`);
        ul.appendChild(li)
    }
});

books.map(function (el,index){
    try {
        searchElement(el, index);
    } catch (error) {
        console.log(error.message);
    }
})

function searchElement(object, index) {

    if (!object.hasOwnProperty('author')) {
        throw new Error(`Объект #${index} в массиве не имеет имя автора`);
    } else if (!object.hasOwnProperty('name')) {
        throw new Error(`Объект #${index} в массиве не имеет названия`);
    } else if (!object.hasOwnProperty('price')) {
        throw new Error(`Объект #${index} в массиве не имеет цены`);
    } else return true;

}

