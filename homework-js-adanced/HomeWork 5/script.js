const urlUsers = `https://ajax.test-danit.com/api/json/users`;
const usersPost = `https://ajax.test-danit.com/api/json/posts`;


fetch(usersPost)
    .then(res => res.json())
    .then(post => {
        post.forEach(post => {
            post = new Card(post.title, post.body, post.userId, post.id)
            post.addPost();
        })
    })

class Card {
    constructor(title, body, userId, id) {
        this.title = title;
        this.body = body;
        this.userId = userId;
        this.id = id;

    }

    addPost() {
        let tweet = document.createElement('div')
        tweet.className = "post-wrapper";
        tweet.id = this.id
        let title = document.createElement('h1')
        title.className = "post-title";
        let text = document.createElement('p')
        text.className = "post-main-text";
        title.textContent = this.title
        text.textContent = this.body
        tweet.append(title, text)
        document.body.append(tweet)
        fetch(`${urlUsers}/${this.userId}`)
            .then(response => response.json())
            .then(user => {
                const userName = document.createElement('h2')
                userName.className = "post-username";
                userName.textContent = `${user.name} ${user.email}`
                tweet.prepend(userName)
            })

        const button = document.createElement('button')
        button.className = "post-delete";
        button.innerText = "Delete post"
        tweet.append(button);
        button.addEventListener("click", function () {
                let postID = tweet.getAttribute("id");
                fetch(`${usersPost}/${postID}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    }
                }).then(response => {
                    if (response.ok) {
                        tweet.remove();

                    }
                })

            }
        )
    }
}

