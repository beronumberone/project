async function ipFinder() {
    const response = await fetch('https://api.ipify.org/?format=json')
    let info = await response.json()
    const information = new UserInfo(info.ip)
    information.letsCheck()
}

class UserInfo {
    constructor(ip) {
        this.ip = ip;
    }

    letsCheck(){
        fetch(`http://ip-api.com/json/${this.ip}`)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                const ul = document.createElement('ul')
                const li = document.createElement('li')
                li.className = 'listIp'
                document.body.append(li)
                li.innerText = (`Your IP: ${this.ip}
                    Country: ${data.country} 
                Country code: ${data.countryCode} 
                Region: ${data.regionName}
                City: ${data.city}`)

            })
    }
}

let checkIpButton = document.getElementById('1')
checkIpButton.addEventListener('click', ipFinder);
