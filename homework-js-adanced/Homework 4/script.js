fetch('https://ajax.test-danit.com/api/swapi/films').then(res => res.json()).then(episodes => {
    const allEpisodes = document.createElement('ul')
    document.body.insertAdjacentElement('afterbegin', allEpisodes)


    episodes.forEach(episode => {
        const episodesOfSerial = document.createElement('li')
        const {episodeId, name, openingCrawl, characters} = episode;
        episodesOfSerial.insertAdjacentHTML('afterbegin',
            `<h1 style="color:orange">Епізод номер: ${episodeId} - ${name}</h1>
                 <p style="color:blue">${openingCrawl}</p>`);


        const charactersOfEpisode = characters.map(character => fetch(character));
        console.log(charactersOfEpisode)
        const episodeCharacters = document.createElement('ul')


        Promise.all(charactersOfEpisode).then(res => Promise.all(res.map(r => r.json()))).then(characters => {
            characters.forEach(character => {
                const {name} = character;
                episodeCharacters.insertAdjacentHTML('beforeend', `<li style="color:red">${name}</li>`)
            })
        })

        episodesOfSerial.append(episodeCharacters);
        allEpisodes.append(episodesOfSerial);

    })
})