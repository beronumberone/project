"use strict"
import gulp from 'gulp';
import clean from 'gulp-clean';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import minifyJs from 'gulp-minify';
import concat from 'gulp-concat';
import imgMin from 'gulp-imagemin';
import prefix from "gulp-autoprefixer";
import minCss from "gulp-clean-css";
import browserSync from "browser-sync";
const sync = browserSync.create();


function moveJs() {
    return gulp.src('./src/scripts/*.js')
        .pipe(concat('script.js'))
        .pipe(minifyJs({
            ext: {
                src: '.js',
                min: 'min.js'
            }, compress: true
        }))
        .pipe(gulp.dest('./dist'))
};


function gulpClean() {
    return gulp.src('./dist/**.*', {read: false})
        .pipe(clean());
};


function minImg() {
    return gulp.src('src/images/**/*')
        .pipe(imgMin())
        .pipe(gulp.dest('dist/img'))
};


function css() {
    return gulp.src('src/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(prefix())
        .pipe(minCss())
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('dist/style'))
};


function browsersync() {
    browserSync.init({
        server: {baseDir: './'},
        notify: false,
        online: true
    })
}

function watcher() {
    gulp.watch('src/sass/**/*.scss', css);
    gulp.watch('src/images/*.*', minImg);
    gulp.watch('src/scripts/*.*', moveJs);
}


gulp.task('browserSync', browsersync);
gulp.task('mini-css', css);
gulp.task('watch', watcher);
gulp.task('moveJs', moveJs);
gulp.task('gulpClean', gulpClean);
gulp.task('minImg', minImg);

gulp.task('build', gulp.series('gulpClean', 'moveJs', 'mini-css', 'minImg'));
gulp.task('dev', gulp.series('watch','browserSync'));
