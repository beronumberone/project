const tabsT = document.querySelectorAll(".tabs-title")
const tabsItems = document.querySelectorAll(".tabs-item")

tabsT.forEach(function (item){
    item.addEventListener("click", function (){
        let currentTab = item;
        let tabId = currentTab.getAttribute('data-tab');
        let currentContent = document.querySelector(tabId)


        tabsT.forEach(function (item){
            item.classList.remove('active')
        })
        tabsItems.forEach(function (item){
            item.classList.remove('active')

        })

        currentTab.classList.add('active')
        currentContent.classList.add('active')
    })
});
