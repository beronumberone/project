window.addEventListener('load', () => {
    toModifyButtons ();
})

const collectionBtn = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
    console.log(event);
    for (let index = 0; index < collectionBtn.length; index++) {
        if (collectionBtn[index].dataset.keyValue.toLowerCase() === event.key.toLowerCase()) {
            toResetColorInButtons();
            collectionBtn[index].style.backgroundColor = 'blue';
        }
    }
})

function toModifyButtons () {
    for (let index = 0; index < collectionBtn.length; index++) {
        collectionBtn[index].dataset.keyValue = `${collectionBtn[index].innerHTML.trim()}`;
    }
}

function toResetColorInButtons() {
    for (let index = 0; index < collectionBtn.length; index++) {
        collectionBtn[index].style.backgroundColor = '';
    }
}