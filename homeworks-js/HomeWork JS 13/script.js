// 1. setTimeout() - делает отсчет времени и выполняет событие один раз через определенное время.
// setInterval() - позволяет выполнять функцию регулярно, через заданное время.

// 2. Если в setTimeout() передать нулевую задержку, то она выполнится настолько бюстро, насколько это возможно,

// 3. clearInterval() нужно вызывать для того, чтобы наша функция остановила работу, иначе она будет продолжать делать заданные действия.

                             
const images = document.querySelectorAll('.image-to-show');
const firstImage = images[0];
const lastImage = images[images.length - 1];
const stopButton = document.querySelector('.stop');
const starButton = document.querySelector('.start');

const slider = () => {
    let currentImage = document.querySelector('.visible');
    if (currentImage !== lastImage) {
        currentImage.classList.remove('visible');
        currentImage.nextElementSibling.classList.add('visible');
    } else {
        currentImage.classList.remove('visible');
        firstImage.classList.add('visible');
    }
}

let timer = setInterval(slider, 3000);

stopButton.addEventListener('click', () => {
    clearInterval(timer);
    starButton.disabled = false;
    stopButton.disabled = true;

})

starButton.addEventListener('click', () => {
    timer = setInterval(slider, 3000);
    starButton.disabled = true;
    stopButton.disabled = false;
})
