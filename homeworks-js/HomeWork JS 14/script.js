const themeSwitch = document.getElementById('color')
let theme = document.getElementById('theme')

themeSwitch.onclick = function () {

    if (theme.getAttribute('href') == 'original-theme.css') {
        theme.href = 'rainbow-theme.css'
        localStorage.setItem('theme', "rainbow")
    } else {
        theme.href = 'original-theme.css'
        localStorage.setItem('theme', "original")
    }
}

window.onload = function () {
    if (localStorage.getItem('theme') === 'rainbow') {
        theme.href = 'rainbow-theme.css'
        localStorage.setItem('theme', "rainbow")
    }
}



