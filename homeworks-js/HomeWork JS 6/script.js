function createNewUser(firstName, lastName) {
    let newUser = {
        name: firstName,
        secondName: lastName,
        getLogin: function () {
            let firstLetter = firstName.charAt(0)
            let login = firstLetter + lastName
            return login.toLowerCase()
        },
        getAge: function (userAge) {
            let ageArr = userAge.split('.')
            let birthday = new Date(`${ageArr[2]}, ${ageArr[1]},${ageArr[0]}`)
            let dif = new Date() - birthday
            let difDays = dif / (60 * 60 * 24 ) / 1000
            let age = Math.floor (difDays / 365)
            return age
        },
        getPassword: function (firstName, lastName, age){
            let upperFirstLetter = firstName.charAt(0).toUpperCase()
            let lowerLastName = lastName.toLowerCase()
            let birthdayDate = age.split('.')
            let birthdayYear = birthdayDate[2]
            let password = upperFirstLetter + lowerLastName + birthdayYear
            return password
        }
    }
    return newUser
}

let firstName = prompt('Введите Ваше имя');
let lastName = prompt('Введите Вашу фамилию')
let userAge = prompt('Введите дату вашего рождения', `dd.mm.yyyy`)

const user = createNewUser(firstName, lastName)


console.log(user.getAge(userAge))
console.log(user.getPassword(firstName, lastName, userAge))
