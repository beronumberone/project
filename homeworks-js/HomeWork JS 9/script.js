// Чтобы создать новый html тэг, нужно воспользоваться document.createElement('div' и т.д)
// insertAdjacentHTML(), position имеет 4 варианта 'beforebegin' до открывающего тэга / 'afterbegin' после открывающего тэга / 'beforeend' перед закрывающим тэгом/ 'afterend' после перед закрывающего тэга
// для удаление элемента со страницы, используем -  document.getElementById('example').remove()

const arr = ['hello', 'my', 'dear', 'friends', 'how are you?']

function parseArr(data) {
    let retstr = '';
    if (typeof data === 'string') {
        retstr += `<li>` + data + `</li>`;
    } else if (Array.isArray(data)) {
        retstr += `<ul>`;
        data.forEach(value => {
            retstr += parseArr(value)
        });
        retstr += `</ul`;
    }
    return retstr;
}

const div = document.getElementById('array');
div.innerHTML = parseArr(arr);
