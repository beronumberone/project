import React from "react";
import './App.css';
import Button from "./Components/Button/Button";
import './Components/Button/Button.scss'
import Modal from "./Components/Modal/Modal";
import {ModalParams} from "./Components/Modal/ModalParams";


class App extends React.Component {
    state = {
        modalId: null
    }

    openModal(id) {
        this.setState({modalId: id})
    }

    closeModal() {
        this.setState({modalId: null})
    }


    render() {
        const {modalId} = this.state
        const modal = ModalParams.find(item => item.id === modalId)
        return (
            <>
                <div className='wrapper-button'>
                    <Button text='Open first modal' backgroundColor='purple' onClick={() => this.openModal(0)}/>
                    <Button text='Open second modal' backgroundColor='darkorange' onClick={() => this.openModal(1)}/>
                </div>
                {modal && <Modal
                    onClick={() => this.closeModal()}
                    key={modal.id}
                    header={modal.headerTitle}
                    text={modal.title}
                    firstBtnText={modal.firstBtnText}
                    secondBtnText={modal.secondBtnText}
                />}
            </>


        )
    }
}

export default App
