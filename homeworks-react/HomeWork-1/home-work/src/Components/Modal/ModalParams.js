export const ModalParams = [
    {
        id: 0,
        title: 'Once you delete this file, it wont be possible to undo this action. Are you sure you want to delete it?',
        headerTitle: 'Do you want to delete this file?',
        firstBtnText: 'Ok',
        secondBtnText: 'Cancel',

    },

    {
        id: 1,
        title: 'So, do you like this window, which give unimportant information?',
        headerTitle: 'Do you like this?',
        firstBtnText: 'Yes',
        secondBtnText: 'Yes?',

    }
]

