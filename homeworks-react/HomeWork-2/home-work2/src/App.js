import './App.css';
import {React, Component} from 'react';
import ProductList from "./Components/ProductList/ProductList";
import ProductCard from "./Components/ProductCard/ProductCard";
import Header from "./Components/Header/Header";
import {ModalParams} from "./Components/Modal/ModalParams";
import Button from "./Components/Button/Button";
import './Components/Button/Button.scss'
import Modal from "./Components/Modal/Modal";


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            favorites: JSON.parse(localStorage.getItem('favorites')) || [],
            cart: JSON.parse(localStorage.getItem('cart')) || [],
            isActive: false,
        };
        this.addToOrder = this.addToOrder.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }

    componentDidMount() {
        fetch('./data.json').then(result => result.json()).then(data => this.setState({products: data.products}))
    }


    addToOrder(item) {
        this.setState({favorites: [...this.state.favorites, item]})
        console.log()
        localStorage.setItem('favorites', JSON.stringify([...this.state.favorites, item]))
    }

    addToCart(item) {

        this.setState({cart: [...this.state.cart, item]})
        localStorage.setItem('cart', JSON.stringify([...this.state.cart, item]))
        console.log(this.state.cart)
        alert('carted')

    }


    render() {
        // const {modalId} = this.state
        // const modal = this.state.products.find(item => item.id === modalId)
        // openModal={(id) => this.openModal(id)}
        // console.log(modal)
        return (
            <>
                <div className='wrapper'>
                    <Header favorites={this.state.favorites} cart={this.state.cart}/>
                    <ProductList products={this.state.products} favorites={this.state.favorites}
                                 onAdd={this.addToOrder}
                                 onCart={this.addToCart}/>
                </div>
                {/*{modal && <Modal*/}
                {/*    products={this.state.products}*/}
                {/*    onCart={this.state.onCart}*/}
                {/*    onClick={() => this.closeModal()}*/}
                {/*    key={modal.id}*/}
                {/*    header={modal.headerTitle}*/}
                {/*    text={modal.title}*/}
                {/*    firstBtnText={modal.firstBtnText}*/}
                {/*    secondBtnText={modal.secondBtnText}/>}*/}
            </>
        )
    }

}


