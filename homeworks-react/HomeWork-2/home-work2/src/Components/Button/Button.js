import React, {Component} from 'react';
import './Button.scss'
import PropTypes from 'prop-types';


class Button extends Component {


    handleClick = () => {
        this.props.onClick()
    }

    render() {
        const {text} = this.props
        return (
            <div>
                <button className='main-list__item--button' onClick={this.handleClick}>{text}</button>
            </div>
        )
    }
}


Button.defaultProps = {
    text: 'Default button',
    onClick: () => {
    }
}

Button.propTypes = {
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ])
}


export default Button;
