import React, {Component} from 'react';
import './header.scss'
import fav from '../../img/favorite.png'
import cart from '../../img/shopping-cart.png'
import PropTypes from "prop-types";
import Button from "../Button/Button";


export class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='header__wrapper'>
                <div className='header__options'>
                    <h1 className='header__options-description'>HQ Fruits for everyone</h1>
                    <img className='header__options-fav' src={fav}/>
                    <p>{this.props.favorites.length}</p>
                    {/*{this.props.favorites.map(favorites => <div>{favorites.id}</div>)}*/}
                    <img className='header__options-cart' src={cart}/>
                    {/*{this.props.cart.map(cart => <div>{cart.id}</div>)}*/}
                    <p>{this.props.cart.length}</p>

                </div>
            </div>
        )
    }
}


export default Header