import React, {Component} from 'react';
import './Modal.scss'
import PropTypes from 'prop-types';

class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='modal__wrapper' onClick={this.handleClick}>
                <div className='modal__body'>
                    <div className='modal__close' onClick={this.props.onClick}>×</div>
                    <div className='modal__header-wrapper'>
                        <h2>{this.props.header}</h2>
                    </div>
                    <hr/>
                    <p>{this.props.text}</p>
                    <div className='modal__btn-wrapper'>
                        <button className='modal__btn-yes' onClick={() => this.props.onCart(this.props.products)}>{this.props.firstBtnText}</button>
                        <button className='modal__btn-no'
                                onClick={this.props.onClick}>{this.props.secondBtnText}</button>
                    </div>
                </div>
            </div>
        )
    }
}


export default Modal