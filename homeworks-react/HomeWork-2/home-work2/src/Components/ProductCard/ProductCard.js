import {Component, React} from "react";
import './productCard.scss'
import favorite from '../../img/favorite.png'
import favoritePick from '../../img/favorite-pick.png'
import {ModalParams} from "../Modal/ModalParams";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import PropTypes from 'prop-types';


class ProductCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalId: null
        };

    }

    openModal(id) {
        this.setState({modalId: id})
    }

    closeModal() {
        this.setState({modalId: null})
    }


    render() {
        const {modalId} = this.state
        const modal = ModalParams.find(item => item.id === modalId)

        let isFav = !this.props.isFavorite ? favorite : favoritePick;
        return (

            <>
                <div className="main-list__item">
                    <div className="main-list__item__image">
                        <img className='main-list__item__image--img' src={this.props.products.image}
                             alt={this.props.products.name}/>
                        <img className='main-list__item__image--fav' src={isFav}
                             onClick={() => this.props.onAdd(this.props.products)}/>
                    </div>
                    <div className="main-list__item__descriptions">
                        <h1 className="main-list__item__descriptions--name">{this.props.products.name}</h1>
                        <p className="main-list__item__descriptions--price">{this.props.products.price} uah</p>
                    </div>
                    <Button text='Add to cart' className='main-list__item--button' onClick={() => this.openModal(0)}/>
                </div>
                {modal && <Modal
                    products={this.props.products}
                    onCart={this.props.onCart}
                    onClick={() => this.closeModal()}
                    key={modal.id}
                    header={modal.headerTitle}
                    text={modal.title}
                    firstBtnText={modal.firstBtnText}
                    secondBtnText={modal.secondBtnText}
                />}

            </>
        )
    }
}

ProductCard.defaultProps = {
    product: {
        name: 'n/a',
        price: 'n/a',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBZDEHae7TZLIJB4l1N0NGYuTej2MaieCXQg&usqp=CAU',
        color: 'n/a'
    }
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
        color: PropTypes.string,
    }),
    onCart: PropTypes.func,
    onAdd: PropTypes.func
}

export default ProductCard