import React, {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import './productList.js.scss'

export class ProductList extends Component {
    render() {
        let favoritesId = this.props.favorites.map(el => el.id)
        return (
            <div className='main'>
                {this.props.products.map(el => (
                    <ProductCard key={el.id} openModal={() => this.props.openModal(el.id)} isFavorite={favoritesId.includes(el.id)} products={el} onAdd={this.props.onAdd} onCart={this.props.onCart}/>))}
            </div>
        )
    }
}


export default ProductList







