import './App.css';
import ProductList from "./Components/ProductList/ProductList";
import Header from "./Components/Header/Header";
import './Components/Button/Button.scss'
import {Routes, Route, Link} from 'react-router-dom';
import MyCart from "./Components/MyCart/MyCart";
import Favorite from "./Components/Favorite/Favorite";
import favorite from "./Components/Favorite/Favorite";
import React, {useState, useEffect} from 'react';


const getFavoritesFromLS = () => {
    let favorites = [];
    if (JSON.parse(localStorage.getItem('favorites'))) {
        return favorites = JSON.parse(localStorage.getItem('favorites'))
    }
    return favorites;
}

const getCartFromLS = () => {
    let cart = [];
    if (JSON.parse(localStorage.getItem('cart'))) {
        return cart = JSON.parse(localStorage.getItem('cart'))
    }
    return cart;
}

const App = () => {
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState(getFavoritesFromLS);
    const [cart, setCart] = useState(getCartFromLS);


    useEffect(() => {
        (async () => {
            const response = await fetch('/data.json');
            const data = await response.json();
            const productsArray = data.products.map(product => {
                let isFavorite = false;
                favorites.forEach(element => {
                    if (element.id === product.id) {
                        isFavorite = true
                    }
                });
                return {...product, isFavorite};
            })
            setProducts(productsArray)
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
        localStorage.setItem('cart', JSON.stringify(cart))
    }, [favorites, cart]);


    // useEffect(() => {
    //     const response = fetch('/data.json');
    //     fetch(response)
    //         .then(res => res.json())
    //         .then(data => setProducts(data))
    //     console.log(products)
    // }, []);

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
        localStorage.setItem('cart', JSON.stringify(cart))
    }, [favorites, cart]);


    let addToOrder = (item, id) => {
        setFavorites([...favorites, item])
        localStorage.setItem('favorites', JSON.stringify([...favorites, item]))
    }

    let addToCart = (item, id) => {

            setCart([...cart, item])
            localStorage.setItem('cart', JSON.stringify([...cart, item]))
            console.log(cart)
            alert('carted')
            // this.setState({cart: [...this.state.cart, item]})
            // localStorage.setItem('cart', JSON.stringify([...this.state.cart, item]))
    }

    let deleteOrder = (id) => {
        console.log(id)
        setCart(cart.filter(el => el.id !== id))
        localStorage.removeItem('cart', JSON.stringify([...cart]));
        // this.setState({cart: this.state.cart.filter(el => el.id !== id)})
        // localStorage.removeItem('cart', JSON.stringify([...this.state.cart]));
    }

    let deleteFavorite = (id) => {
        console.log(favorites)
        setFavorites(favorites.filter(el => el.id !== id))
        // this.setState({favorites: this.state.favorites.filter(el => el.id !== id)})
        // console.log(this.state.favorites)

    }


    return (
        <>
            <Header favorites={favorites} products={products} cart={cart}/>
            {/*<Link to='/'>Hello</Link>*/}
            {/*<Link to='/favorite'>fav</Link>*/}
            {/*<Link to='/cart'>cart</Link>*/}

            {/*<div className='wrapper'>*/}
            {/*    <Header favorites={favorites} cart={cart}/>*/}
            {/*    <ProductList products={products} favorites={favorites}*/}
            {/*                 addToOrder={addToOrder}*/}
            {/*                 addToCart={addToCart}/>*/}
            {/*</div>*/}

            <Routes>
                <Route path='/'
                       element={<ProductList products={products} favorites={favorites} cart={cart}
                                             addToOrder={addToOrder}
                                             addToCart={addToCart}/>}/>
                <Route path='/favorite'
                       element={<Favorite favorites={favorites} deleteFavorite={deleteFavorite}/>}/>
                <Route path='/cart' element={<MyCart cart={cart} deleteOrder={deleteOrder}/>}/>
            </Routes>
        </>
    )
}

export default App