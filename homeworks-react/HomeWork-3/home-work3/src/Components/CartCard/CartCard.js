import {Component, React} from "react";
import favoritePick from "../../img/favorite-pick.png";
import './CartCard.scss'
import FavoriteCard from "../FavoriteCard/FavoriteCard";
import favorite from "../../img/favorite.png";
import Modal from "../Modal/Modal";
import {ModalParams} from "../Modal/ModalParams";

export class CartCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalId: null
        };

    }

    openModal(id) {
        this.setState({modalId: id})
    }

    closeModal() {
        this.setState({modalId: null})
    }

    render() {
        const {modalId} = this.state
        const modal = ModalParams.find(item => item.id === modalId)
        return (
            <div>
                <div className="cart-list__item">
                    <div className="cart-list__item__image">
                        <img className='cart-list__item__image--img' src={this.props.cart.image}
                             alt={this.props.cart.name}/>
                    </div>
                    <div className="cart-list__item__descriptions">
                        <h1 className="cart-list__item__descriptions--name">{this.props.cart.name}</h1>
                        <p className="cart-list__item__descriptions--price">{this.props.cart.price} uah</p>
                    </div>
                    <button className="cart-list__item__descriptions--delete"
                            onClick={() => this.props.deleteOrder(this.props.cart.id)}>Delete from the cart?
                    </button>
                    {/*onClick={() => this.openModal(1)}>Delete from the cart?*/}

                </div>
                {modal && <Modal
                    products={this.props.products}
                    onCart={this.props.onCart}
                    onClick={() => this.closeModal()}
                    key={modal.id}
                    header={modal.headerTitle}
                    text={modal.title}
                    firstBtnText={modal.firstBtnText}
                    secondBtnText={modal.secondBtnText}
                />}
                {/*{this.props.favorites.name}*/}
            </div>

        )
    }
}


export default CartCard