import './favorite.scss'
import React, {Component} from 'react';
import FavoriteCard from "../FavoriteCard/FavoriteCard";



const Favorite = ({favorites, deleteFavorite}) => {
        return (
            <>
                <h1 className='cart__title'>Hello, we are your favorite fruits &hearts; </h1>

                <div className='main__fav--wrapper'>
                    {favorites.map(el => (
                        <FavoriteCard key={el.id} favorites={el} deleteFavorite={deleteFavorite}/>
                    ))}
                </div>

            </>
        )
}


export default Favorite;