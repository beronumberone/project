import {Component, React} from "react";
import favoritePick from "../../img/favorite-pick.png";

import './FavoriteCard.scss'

const FavoriteCard = ({favorites, deleteFavorite}) => {


    return (
        <div>
            <div className="favorite-list__item">
                <div className="favorite-list__item__image">
                    <img className='favorite-list__item__image--img' src={favorites.image}
                         alt={favorites.name}/>
                    <img className='favorite-list__item__image--fav' src={favoritePick}
                         onClick={() => deleteFavorite(favorites.id)}/>
                    {/*     onClick={() => this.props.onAdd(this.props.favorites)}/>*/}
                </div>
                <div className="favorite-list__item__descriptions">
                    <h1 className="favorite-list__item__descriptions--name">{favorites.name}</h1>
                    <p className="favorite-list__item__descriptions--price">{favorites.price} uah</p>
                </div>
            </div>
        </div>
    )
}

// class FavoriteCard extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             modalId: null
//         };
//
//     }
//
//     render() {
//         const {modalId} = this.state
//         const modal = ModalParams.find(item => item.id === modalId)
//
//         let isFav = !this.props.isFavorite ? favorite : favoritePick;
//         return (
//
//             <>
//                 <div className="main-list__item">
//                     <div className="main-list__item__image">
//                         <img className='main-list__item__image--img' src={this.props.favorites.image}
//                              alt={this.props.favorites.name}/>
//                         {/*<img className='main-list__item__image--fav' src={isFav}*/}
//                         {/*     onClick={() => this.props.onAdd(this.props.favorites)}/>*/}
//                     </div>
//                     <div className="main-list__item__descriptions">
//                         <h1 className="main-list__item__descriptions--name">{this.props.favorites.name}</h1>
//                         <p className="main-list__item__descriptions--price">{this.props.favorites.price} uah</p>
//                     </div>
//                 </div>
//                 {/*{modal && <Modal*/}
//                 {/*    products={this.props.products}*/}
//                 {/*    onCart={this.props.onCart}*/}
//                 {/*    onClick={() => this.closeModal()}*/}
//                 {/*    key={modal.id}*/}
//                 {/*    header={modal.headerTitle}*/}
//                 {/*    text={modal.title}*/}
//                 {/*    firstBtnText={modal.firstBtnText}*/}
//                 {/*    secondBtnText={modal.secondBtnText}*/}
//                 {/*/>}*/}
//
//             </>
//         )
//     }
// }
//
export default FavoriteCard