import React, {Component} from 'react';
import './header.scss'
import fav from '../../img/favorite.png'
import cart from '../../img/shopping-cart.png'
import shop from '../../img/shop.png'
import {Outlet, NavLink, Link} from 'react-router-dom'


export class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <div className='header__wrapper'>
                    <div className='header__options'>
                        <h1 className='header__options-description'>HQ Fruits for everyone</h1>

                        <Link to='/'><img className='header__options-fav' src={shop}/></Link>
                        <Link to='/favorite'><img className='header__options-fav'  src={fav}/></Link>
                        <Link to='/cart'><img className='header__options-cart' src={cart}/></Link>

                        {/*<img className='header__options-fav' src={shop}/>*/}
                        {/*<img className='header__options-fav'  src={fav}/>*/}
                        {/*<p>{this.props.favorites.length}</p>*/}
                        {/*<img className='header__options-cart'  src={cart}/>*/}
                        {/*<p>{this.props.cart.length}</p>*/}
                    </div>
                </div>
            </>
        )
    }
}


export default Header