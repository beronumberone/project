export const ModalParams = [
    {
        id: 0,
        title: 'We are happy for your choice <3',
        headerTitle: 'Do you want to add this fruit to your cart?',
        firstBtnText: 'Yes',
        secondBtnText: 'No :( ',

    },
    {
        id: 1,
        title: 'Ohhhh, so bad :(',
        headerTitle: 'Do you want to delete this fruit from your cart?',
        firstBtnText: 'Yes :(',
        secondBtnText: 'No',

    }
]

