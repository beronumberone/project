import './myCart.scss'
import React, {Component} from 'react';
import CartCard from "../CartCard/CartCard";

export class MyCart extends Component {
    render() {
        return (
            <>
                <h1 className='cart__title'>Hello, we are your cart fruits &hearts; </h1>
                <div className='main-wrapper'>
                    {this.props.cart.map(el => (
                        <CartCard key={el.id} cart={el} deleteOrder={this.props.deleteOrder}/>
                        // <h1>{el.name}</h1>
                    ))}
                </div>
            </>
        )
    }
}


export default MyCart;