import {Component} from "react";
import './productCard.scss'
import favorite from '../../img/favorite.png'
import favoritePick from '../../img/favorite-pick.png'
import {ModalParams} from "../Modal/ModalParams";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';


const ProductCard = ({products, favorites, addToOrder, addToCart, isFavorite}) => {
    const [modalId, setModalId] = useState(null);

    let openModal = (id) => {
        this.setState({modalId: id})
    }

    let closeModal = () => {
        this.setState({modalId: null})
    }


    // const {modalid} = modalId
    // const modal = ModalParams.find(item => item.id === modalId)
    //
    let isFav = !isFavorite ? favorite : favoritePick;

    return (
        <>
            <div className="main-list__item">
                <div className="main-list__item__image">
                    <img className='main-list__item__image--img' src={products.image}
                         alt={products.name}/>
                    <img className='main-list__item__image--fav' src={isFav}
                         onClick={() => addToOrder(products)}/>
                </div>
                <div className="main-list__item__descriptions">
                    <h1 className="main-list__item__descriptions--name">{products.name}</h1>
                    <p className="main-list__item__descriptions--price">{products.price} uah</p>
                </div>
                <Button text='Add to cart' className='main-list__item--button' onClick={() => addToCart(products)}/>
            </div>
            {/*{modal && <Modal*/}
            {/*    products={products}*/}
            {/*    onCart={onCart}*/}
            {/*    onClick={() => this.closeModal()}*/}
            {/*    key={modal.id}*/}
            {/*    header={modal.headerTitle}*/}
            {/*    text={modal.title}*/}
            {/*    firstBtnText={modal.firstBtnText}*/}
            {/*    secondBtnText={modal.secondBtnText}*/}
            {/*/>}*/}

        </>
    )


}

ProductCard.defaultProps = {
    product: {
        name: 'n/a',
        price: 'n/a',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBZDEHae7TZLIJB4l1N0NGYuTej2MaieCXQg&usqp=CAU',
        color: 'n/a'
    }
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
        color: PropTypes.string,
    }),
    addToCart: PropTypes.func,
    addToOrder: PropTypes.func
}

export default ProductCard