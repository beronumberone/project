import {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import './productList.js.scss'
import App from "../../App";
import React, {useState, useEffect} from 'react';

    const ProductList = ({products, favorites, addToOrder, addToCart}) => {
        let favoritesId = favorites.map(el => el.id)
        return (
            <div className='main'>
                {products.map(el => (
                    <ProductCard key={el.id} openModal={() => this.props.openModal(el.id)}
                                 isFavorite={favoritesId.includes(el.id)} products={el} addToOrder={addToOrder}
                                 addToCart={addToCart}/>))}
            </div>
        )

}

export default ProductList









