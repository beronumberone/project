import {createBrowserRouter} from "react-router-dom";

import Header from "../Components/Header/Header";

import ProductCard from "../Components/ProductCard/ProductCard";


const Router = createBrowserRouter(
    [
        {
            path: '/',
            element: <Header/>,
            children: [
                {path: '/', element: <ProductCard/>},
                // {path: 'product-card', element: <ProductCard/>},
                // {path: 'contacts', element: <Contacts/>},
                // {path: 'how-are-you', element: <HowAreYou/>},
            ]
        },
        // {path: '*', element: <PageNotFound/>},
    ]);


export default Router;