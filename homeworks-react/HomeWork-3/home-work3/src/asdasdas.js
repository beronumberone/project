


import './App.css';
import React, {useState, useEffect} from 'react';
import ProductList from "./Components/ProductList/ProductList";
import Header from "./Components/Header/Header";
import './Components/Button/Button.scss'
import {Routes, Route} from 'react-router-dom';
import MyCart from "./Components/MyCart/MyCart";
import Favorite from "./Components/Favorite/Favorite";
import favorite from "./Components/Favorite/Favorite";

const getFavoritesFromLS = () => {
    let favorites = [];
    if (JSON.parse(localStorage.getItem('favorites'))) {
        return favorites = JSON.parse(localStorage.getItem('favorites'))
    }
    return favorites;
}

const getCartFromLS = () => {
    let cart = [];
    if (JSON.parse(localStorage.getItem('cart'))) {
        return cart = JSON.parse(localStorage.getItem('cart'))
    }
    return cart;
}

const App = () => {
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState(getFavoritesFromLS());
    const [cart, setCart] = useState(getCartFromLS());

    useEffect(() => {
        (async () => {
            const response = await fetch('/data.json');
            const data = await response.json();
            const productsArray = data.products.map(product => {
                let isFavorite = false;
                favorites.forEach(element => {
                    if (element.id === product.id) {
                        isFavorite = true
                    }
                });
                return {...product, isFavorite};
            })
            setProducts(productsArray)
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
        localStorage.setItem('cart', JSON.stringify(cart))
    }, [favorites, cart]);

    const onCartAddHandleClick = (id) => {
        const product = products.filter(product => {
            return product.id === id
        })

        const isCartData = cart.some(product => product.id === id)

        if (!isCartData) {
            setCart([...cart, ...product]);
        }

    }

    const onCartDeleteHandleClick = (id) => {
        const cartArray = cart.filter(product => {
            return product.id !== id
        })
        setCart(cartArray);

    }


    const addToOrder = (item) => {
        this.setState({favorites: [...this.state.favorites, item]})
        console.log()
        localStorage.setItem('favorites', JSON.stringify([...this.state.favorites, item]))
    }

    const addToCart = (item) => {
        this.setState({cart: [...this.state.cart, item]})
        localStorage.setItem('cart', JSON.stringify([...this.state.cart, item]))
        console.log(this.state.cart)
        alert('carted')

    }


    const onFavoritesClickHandle = (id) => {

        const product = favorites.filter(product => {
            return product.id === id
        })

        if (product.length === 0) {
            const product = products.filter(product => {
                return product.id === id
            })

            const newArrayFavorites = [...favorites, ...product];
            const newArrayProducts = products.map((product) => {
                if (product.id === id) {
                    product.isFavorite = !product.isFavorite;
                }
                return product
            })

            setFavorites(newArrayFavorites);
            setProducts(newArrayProducts);

        } else {
            const newArrayFavorites = favorites.filter((favorite) => {
                return favorite.id !== id;
            })
            const newArrayProducts = products.map((product) => {
                if (product.id === id) {
                    product.isFavorite = !product.isFavorite;
                }
                return product
            })
            setFavorites(newArrayFavorites);
            setProducts(newArrayProducts);

        }
    }


    console.log(products)
    return (
        <>

            <Header favorites={favorites} cart={cart}/>

            <Routes>
                <Route path='/'
                       element={<ProductList products={products} favorites={favorites}
                                             addToOrder={addToOrder}
                                             addToCart={addToCart}/>}/>
                {/*<Route path='/favorite'*/}
                {/*       element={<Favorite favorites={this.state.favorites} deleteFavorite={this.deleteFavorite}/>}/>*/}
                {/*<Route path='/cart' element={<MyCart cart={this.state.cart} deleteOrder={this.deleteOrder}/>}/>*/}
            </Routes>

            <p>sss</p>
        </>
    )


}

export default App


